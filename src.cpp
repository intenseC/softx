/*000001*/
/*000002*/
/*000003*/
/*000004*/
/*000005*/
/*000006*/
/*000007*/
/*000008*/

/*  ESP82xx  winCLI terminal utility template by Simon 'intenseC'   */
/*  2019  */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <conio.h>
#include <ctype.h>
//#include <string.h>
#include <assert.h>
//#include "timer/timer.h"
#include <windows.h>

    #define _SFVERSION  "004b"
    //      #define _DEBUG
	//      #define _HELLOcHINA

#define _BAUD  57600UL   // 57600UL  //   CBR_115200
#define __FILESZ 131072
#define __PAGESZ 2048UL

       // Declare variables and structures
        FILE *f, *fp, *ft;

	  const uint8_t rig = 8;
	  const char * easterEgg = "firm.bin"; 
	//TODO: create buffer and read commands to it from text received
  const char * const cmdHlp = 
  "\n\r                ***  Brief Command List  ***\n\n\
   SRVR_START [ssid] [pwd]- connect to an existing wifi\n\
   SPIFFS_START [-noKEYS] - start preconfigured server (also [s])\n\
   SERVER_RST [-noKEYS]  - reset the chip\n\
   SPIFFS_LST [-noKEYS]  - list all the SPIFFS files (also [f])\n\
   WIFI_LIST [-noKEYS] (also [l])\n\
   SPIFFS_DEL -  delete a file. format: SPIFFS_DEL [/filename.ext] [-noKEYS]\n\
   AP_START [-noKEYS] - Enable Access Point \n\
   \n\
   \n\
   \n\r\
  ";

    #define MAXUARTDEV 90

    uint8_t scan_max = MAXUARTDEV;
    uint8_t scan_min = 1;
	uint8_t portQue = MAXUARTDEV, doOnce = 0;
	
   char dev_name[MAX_PATH] = "";
   int dev_number = -1;

	HANDLE hSerial;
    DCB dcbSerialParams = {0};
    COMMTIMEOUTS timeouts = {0};

     DWORD bytes_read, bytes_written, total_bytes_written = 0;
 
     uint32_t binSz = 0;
     uint32_t stage = 0;
	 uint16_t scaler = 1;
	 uint16_t dScaler = 0;
     const uint32_t memSize = 8192; 
	 int8_t arr[__FILESZ];   //  131100  // 128kB
     uint8_t bRec[32768];
     int8_t rec[32768];
     int8_t payL[262144];
	
	  uint8_t   uBuf[3000] = {0};
	  uint32_t sz;
	  uint16_t playQue = 0;
//  ====  proto-s
void animatedBar(void);
void CloseSerialPort(void);
void write2port(void);
void progressBar(double whole, double fract);
void show_status(double percent);
//========================================================
/**/
void helloChina(uint8_t msg) {
   #if 0
 		 //======================  
		  uint8_t * tcmd = "t0.txt=\"CHINESE CODING IS A NO-GO\"";
		  uint8_t c = 0xff; 
		  if(msg)
		  WriteFile(hSerial, tcmd, strlen(tcmd), &bytes_written, NULL);
		  WriteFile(hSerial, &c, 1, &bytes_written, NULL);
		  WriteFile(hSerial, &c, 1, &bytes_written, NULL);
		  WriteFile(hSerial, &c, 1, &bytes_written, NULL);
         //=========================
   #endif
 }
//========================================================

void draWebSockets(void)
{      
	char sendSignal[28] = {0};
	static uint16_t  cntr0,  rev;
	uint16_t y = 0;
        if(cntr0 < 100 && !rev) cntr0 += 10; else
		   { rev = 1; if((cntr0 -= 10) == 0) rev = 0; } 

			  //strcat((const char*)val, sendSignal);  
			  sprintf(sendSignal, "sDRWG -void %d\n\r", cntr0 / 10);
			  printf(sendSignal);
			  WriteFile(hSerial, sendSignal, 28, &bytes_written, NULL);
			  memset(sendSignal, 0, sizeof sendSignal);
			ReadFile(hSerial, rec, 255, &bytes_read, NULL);
            if(bytes_read) { 
				          WriteFile(hSerial, "\r\n", 2, &bytes_written, NULL); 	  
			} 
}

#if 1
void espRawSpeed()
{
	static uint8_t runOnce;  // mutex
	if(runOnce) return;
    dcbSerialParams.BaudRate = 2000000UL;  //     CBR_115200
    dcbSerialParams.ByteSize = 8;  //   7
    dcbSerialParams.StopBits = ONESTOPBIT; //    TWOSTOPBITS
    dcbSerialParams.fParity = FALSE;
    dcbSerialParams.Parity = NOPARITY;
    dcbSerialParams.fDtrControl = 0;
    dcbSerialParams.fRtsControl = 0;

	if(SetCommState(hSerial, &dcbSerialParams) == 0)
    {
        fprintf(stderr, "Error setting device parameters\n");
        CloseHandle(hSerial);
    }
    runOnce = 1;
}
#endif

void  espTalk(uint8_t com) {
		static uint32_t err, pkts, cntr, errcntr, onFlag, onFlagE;
		uint32_t s, n = 0;
		uint16_t y = 0;
		static uint16_t  cntr0,  rev;
		char * rcv, *p, cmd[13] =  { 0xff, 0x5f, 0x57, 0x41, 0x4b, 0x45, 0x55,\
		0x50, 0x5f, 0x5f, 0x5f, 0x5f, 0x5f },
		hdr[4] = { 0xfa, 0xfb, 0xfa, 0xfb };
      	char val[8], str[220] = {0};  
        memset(rec, 0, sizeof rec);
        char sendSignal[28] = {0};

 		printf("\n     ");

   switch(com) {
        
      case '1':
              draWebSockets();
	  #if 0
			  if(cntr0 < 100 && !rev) cntr0 += 10; else
		      { rev = 1; if((cntr0 -= 10) == 0) rev = 0; } 
                               y = cntr0 / 10;
			  sprintf(sendSignal, "sDRWG -void %d\n\r", y);
			  printf(sendSignal);
			   WriteFile(hSerial, sendSignal, 28, &bytes_written, NULL);

	  #endif
      break;
      
	  case '2':
      break;

	  case '3':
	           sprintf(sendSignal, "sDRWF  -void\n\r");
			   printf(sendSignal);
			   WriteFile(hSerial, sendSignal, 28, &bytes_written, NULL);
      break;

	  case '4':
	           sprintf(sendSignal, "sDRWS  -void\n\r");
			   printf(sendSignal);
			   WriteFile(hSerial, sendSignal, 28, &bytes_written, NULL);
      break;

	  case 's':
	           sprintf(sendSignal, "svrSTR -void\n\r");
			   printf(sendSignal);
			   WriteFile(hSerial, sendSignal, 28, &bytes_written, NULL);
      break;

	  case 'l':
	           sprintf(sendSignal, "wfLS -void\n\r");
			   printf(sendSignal);
			   WriteFile(hSerial, sendSignal, 28, &bytes_written, NULL);
      break;

	  case 'f':
	           sprintf(sendSignal, "fsLS -void\n\r");
			   printf(sendSignal);
			   WriteFile(hSerial, sendSignal, 28, &bytes_written, NULL);
      break;

	  case 'h':
		animatedBar(); printf("\n\r"); 
		sprintf(sendSignal, "statREP -void\n\r");
		printf(sendSignal);
		WriteFile(hSerial, sendSignal, strlen(sendSignal), &bytes_written, NULL);
        printf("     ");	animatedBar(); printf("\n\r"); return;
      break;

     }  /* switch out */
     		
       memset(sendSignal, 0, sizeof sendSignal);  		  printf("\n\r");
    #if 1
            ReadFile(hSerial, rec, sizeof rec, &bytes_read, NULL);
            if(bytes_read) { 
				for(n = 0; n < bytes_read; n += 1)
				{
								printf("%01c", rec[n]);	
				}
                 printf("\n\n     %d  bytes received.\n\n", bytes_read); 
	#if 0
				for(n = 0; n < bytes_read; n += 1)
				{ printf("%c ", rec[n]); if(n && !(n % 25)) 
				printf("\n"); }
	#endif
				} 
             WriteFile(hSerial, "\n\r", 2, &bytes_written, NULL);  
			 // esp8266 waits for "\n\r" per confirmation
    #endif
 }

void CloseSerialPort() {
    if (hSerial != INVALID_HANDLE_VALUE)
    {
        // Close serial port
        fprintf(stderr, "\n Closing serial port...");
        if (CloseHandle(hSerial) == 0)
            fprintf(stderr, " Error\n");
        else fprintf(stderr, " OK\n");
    }
}


// simple binary file handler
  void   openBin(void)
    {
    uint32_t i = 0, y = 0, nmlen = 0;
	char ext[5]   = {0};
	char str[220] = {0};
    char fnm[220] = {0};
	char con, sym, *p;
		 fp = fopen(easterEgg, "rb");
		   if(fp == NULL)
		{
   printf("\n\n     Please Put Your Encrypted File in the Same Directory,  \n \
    Type its Name (Including Extension) And Press Enter \n "); 
   printf("\n     ");
    fgets(str, sizeof str, stdin);  y = i = strlen(str) - 1; 
     if((p = strchr(str, '\n')) != NULL) { *p = '\0'; /* remove newline sign*/ }
       sym = str[y - 1];
 	#if 1   // handle files with spaces in their path
		//==================================================
	    // file was dragged into console window and has spaces in its path
	    // and hence it also has quotation mark at margins
	   if(sym == '"') 
		  { y -= 2;  //remove (") and ajust to the last array cell
	while(str[y] != '\\') { /* printf("%c\n", str[y]); */ 
		fnm[nmlen] = str[y]; nmlen++; y--;   }
	strrev(fnm); memset(str, 0, sizeof str);
	strcpy(str, fnm); i = strlen(fnm);
	}
		//==================================================
	#endif
   if(strlen(str) < 5 || str[i - 4] != '.')
    {
        printf(" \n  Bad File Name(s)\n  Hit a Key to Exit");  printf("\n");
            if(getch())  exit(1); 
    }
                  // copy user provided file extension 
                      ext[3] = str[i - 1];
                      ext[2] = str[i - 2];
                      ext[1] = str[i - 3];
                      ext[0] = str[i - 4];
	#if 0  // DEBUG
	 	           printf(ext); printf(" "); printf(str);
	#endif
		 fp = fopen(str, "rb");
                                if(fp == NULL) {
             printf(" \n  Error Opening File(s)\n  Hit a Key to Exit");  
			 printf("\n"); if(getch())    exit(1); }
				  } // end if
     	else
		{ printf(" \n\n    You're Lucky Today, Firmware [%s] found! \n\n", easterEgg); }
		
		 fread(payL, sizeof(payL), 1, fp);
		 fseek(fp, 0, SEEK_END);
		 sz = ftell(fp); 
           printf("      \n     File Size is %d kB... \n\n", sz);
    }

  // STM32 UART programming module
void 
   startComm(void) {
        
       DCB dcb;
	unsigned char chRead;

		     OVERLAPPED o;
		   DWORD dwEvtMask, modStat;   
		   
		   o.hEvent = CreateEvent(
        NULL,   // default security attr 
        FALSE,  // event autoreset 
        FALSE,  // state to signal 
        NULL    // no name
		);  
		   
		   uint32_t s, n = 0;
		char rcv, cmd[5] = {0};
      	char str[220] = {0};
	    cmd[0] = 'q';
								s = (sz % 2048) ? (sz / 2048) + 1 : sz / 2048;
                                s = (s > 120) ? 120 : s;     // 120 pages tops
					   printf("\n\n     Pages to Program: %d\n", s + 1);


			WriteFile(hSerial, cmd, 1, &bytes_written, NULL);
				ReadFile(hSerial, &rcv, 1, &bytes_read, NULL);

	#if 1 
									 if(bytes_read == 0) {  // read timeout
						printf("\n\n     Communication Error, Please Try Reconnecting Your USB Device, \n ");
						printf("    While Holding DFU Button Pressed\n\n ");
						printf("\n\n     Press any Key to Exit or Enter to Ignore Error above\n\n");
								 if(getch() != '\r')  exit(1);  else goto skipChk;
								 } else
			           if(bytes_read == 1 && rcv != 'q')  { 
								printf("\n\n     Communication Error, Press any Key to Exit\n\n");
								printf("error, %c", rcv); 
								if(getch()) exit(1);	 
								}	
						skipChk:
				 	    #endif
				//========================================
				if(!GetCommState(hSerial, &dcb))  printf("errC");
				//========================================
				SetCommMask(hSerial, EV_BREAK | EV_ERR | EV_RXCHAR);
			printf("    \n     Hit Enter to Start Programming, any Other Key to Exit \n ");
            if(getch() != '\r') exit(1);
					cmd[0] = 'a'; cmd[1] = s;  cmd[2] = 'd'; 
					WriteFile(hSerial, cmd, 3, &bytes_written, NULL);
					   ReadFile(hSerial, &rcv, 1, &bytes_read, NULL);
								 if(bytes_read == 0) {  // read timeout
						printf("\n\n Communication Error, Press any Key to Exit\n\n");
								 if(getch()) exit(1);
								 } else 
							 if(bytes_read == 1 && rcv != 'q') 
								  {
                                  printf("\n\n ERR \n\n"); 
                 if(!GetCommModemStatus(hSerial, &modStat))  printf("errM");
						          }
				 printf("    \n    Sending File... \n\n ");
						for(n = 0; n <= s; n += 1)
						 {
	#if 1
				WriteFile(hSerial, payL + (n * 2048), __PAGESZ, &bytes_written, NULL);
					progressBar(s, n);	
					 ReadFile(hSerial, &rcv, 1, &bytes_read, NULL);
                    	if(bytes_read == 1 && rcv == 'Q')   { 
					     printf("    \n\n     Nicely Done!  All Data Has Been Sent Successfully. \n");	break; }     
						   else if(bytes_read == 1 && rcv != 'q') break;
    #else
	#endif
						 }
        printf(" \n\n                  Now Hit a Key to Exit.");     printf("\n");  
		if(getch())    exit(1);
   }

 void  uartHandShk(void) {
    HANDLE hCom;
    OVERLAPPED o;
    BOOL fSuccess;
    DWORD dwEvtMask;

		   o.hEvent = CreateEvent(
        NULL,   // default security attr 
        FALSE,  // event autoreset 
        FALSE,  // state to signal 
        NULL    // no name
		);  
		   
		   uint16_t n = 0;
		char rcv, cmd[5] = {0};
      	char str[220] = {0};
	    cmd[0] = 'q';
    portQue = portQue <= 0 ? MAXUARTDEV : portQue;
      // Scan for an available COM port in _descending_ order
    printf("    \n     Scanning COM Ports... \n\n");
	for(n = portQue; n >= scan_min; --n)
    {
        // Try next device
        sprintf(dev_name, "\\\\.\\COM%d", n);
        hSerial = CreateFile(dev_name, GENERIC_READ|GENERIC_WRITE,
        0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
        if(hSerial != INVALID_HANDLE_VALUE)
        {

             printf(  "     Opening COM%d...", n);
			 fprintf(stderr, "     OK!");
             printf( "    Baudrate: %u \n",  _BAUD);
            portQue = dev_number = n;
            break; // stop scanning COM ports
        }
        else /*if (debug > 1)*/ 
		{ 
		   if(n == scan_min) {
               fprintf(stderr, "    --- NO AVAILIABLE COM PORT FOUND! ---\n");
		  printf("    \n    Hit any Key to Exit, [p] to scan again \n ");
          if(getch() != 'p') exit(1);  else {   portQue = MAXUARTDEV + 1;  uartHandShk();
            } 
          }
		}
      }

  #if 1
    dcbSerialParams.DCBlength = sizeof( dcbSerialParams );
  #ifdef _HELLOcHINA
	dcbSerialParams.BaudRate = CBR_38400;  //CBR_115200;
  #else
	dcbSerialParams.BaudRate = _BAUD;  //
  #endif
    dcbSerialParams.ByteSize = 8;  // 7?
    dcbSerialParams.StopBits = ONESTOPBIT; //TWOSTOPBITS
    dcbSerialParams.fParity = FALSE;
    dcbSerialParams.Parity = NOPARITY;
    dcbSerialParams.fDtrControl = 0;
    dcbSerialParams.fRtsControl = 0;
    //
  #if 1
	dcbSerialParams.XoffChar = 19;
	dcbSerialParams.XonChar = 17;
	dcbSerialParams.XonLim = 2048;
	// dcbSerialParams.XoffLim = 1024;	  //512
	dcbSerialParams.XoffLim = 512;	 
   #endif  
   #if 1
    // Set COM port timeout settings
    timeouts.ReadIntervalTimeout = 50;
    timeouts.ReadTotalTimeoutConstant = 50;
    timeouts.ReadTotalTimeoutMultiplier = 50;
    timeouts.WriteTotalTimeoutConstant = 50;
    timeouts.WriteTotalTimeoutMultiplier = 50;
    if(SetCommTimeouts(hSerial, &timeouts) == 0)
    {
        fprintf(stderr, "Error setting timeouts\n");
        CloseHandle(hSerial);
    } 
	  
	#endif		  
				  
	#if 0
		GetCommMask(hSerial, EV_CTS | EV_DSR | EV_RING | EV_RLSD | EV_RXCHAR | 
		EV_TXEMPTY | EV_BREAK);
	    WaitCommEvent(hSerial, EV_CTS | EV_DSR | EV_RING | EV_RLSD | EV_RXCHAR | 
		EV_TXEMPTY | EV_BREAK, NULL);

	dcbSerialParams.fOutxCtsFlow = false;
    dcbSerialParams.fRtsControl = 0x00;
    dcbSerialParams.fOutX = true;
    dcbSerialParams.fInX = true;

	dcbSerialParams.fOutX = TRUE;
    dcbSerialParams.fInX = TRUE;
	
	dcbSerialParams.fBinary = FALSE;
    dcbSerialParams.fParity = FALSE;
    dcbSerialParams.fOutxCtsFlow = FALSE;
    dcbSerialParams.fOutxDsrFlow = FALSE;
    dcbSerialParams.fDsrSensitivity = FALSE;
    dcbSerialParams.fErrorChar = FALSE;
    dcbSerialParams.fNull = FALSE;
    dcbSerialParams.fAbortOnError = FALSE;
	      #endif
	
	if(SetCommState(hSerial, &dcbSerialParams) == 0)
    {
        fprintf(stderr, "Error setting device parameters\n");
        CloseHandle(hSerial);
    }
	

	SetCommMask(hSerial, EV_CTS | EV_DSR | EV_RING | EV_RLSD | EV_RXCHAR |
		EV_TXEMPTY | EV_BREAK);
				 #if(1)		
	o.Internal = 0;
    o.InternalHigh = 0;
    o.Offset = 0;
    o.OffsetHigh = 0;

    assert(o.hEvent);		  
	#endif		  
	#if 0
				  printf(" \n\n Press DFU Button to Begin \n\n ");

		 if(WaitCommEvent(hSerial, &dwEvtMask, &o)) 
    {
        if(dwEvtMask & (EV_CTS | EV_DSR | EV_RING | EV_RLSD | EV_RXCHAR | EV_TXEMPTY | EV_BREAK))

        {
          startComm();
		 printf(" \n\n err \n\n ");    // 
        }
    }		  
	  #endif
      #endif
	  
	  #if 0   // fw burn
          printf("    \n    Hit Enter to Start, any Other Key to Exit \n ");
          if(getch() != '\r') exit(1);
                       printf("OK");
         if(!WriteFile(hSerial, cmd, 1, &bytes_written, NULL))
    {
        fprintf(stderr, "Error\n");
        CloseHandle(hSerial);
       
    } 

		ReadFile(hSerial, &rcv, 1, &bytes_read, NULL);
	     if(bytes_read == 1)
            {
                   printf("%c", rcv);
                if(rcv = 'q')   // ack
				{      
					cmd[0] = 'a'; cmd[1] = sz;  cmd[2] = 'd'; 
					WriteFile(hSerial, cmd, 3, &bytes_written, NULL);
				}
            }
         ReadFile(hSerial, &rcv, 1, &bytes_read, NULL);
	     if(bytes_read == 1 && rcv == 'q')
            {     
				 printf("    \n  Sending... \n ");	 
					 for(n = 0; n < sz; n++) 
						 {
	   WriteFile(hSerial, payL + (n * __PAGESZ), __PAGESZ, &bytes_written, NULL);
						  printf("    \n  Page %d Sent \n ", n); 
						 }
						  printf("    \n\n  All Sent! \n");
        printf(" \n\n                  Now Hit a Key to Exit.");     printf("\n");  
		if(getch())    exit(1);
            }
   #endif
    }


  void  progressBar(double whole, double fract) {
        
    double x, y;
	y = (fract / whole) * 100;
	printf("    ");  
	printf("Progress:  %05.2f%% [",  y);
    for(x = 0; x < (y / 2); x++)  printf("+");
    for(x = (y / 2); x < 50; x++) printf(" ");
	  printf("]"); 
	  printf("\r");
      Sleep(1);
   }


void show_status(double percent) {
    
    int x, y;
	if(percent == 100) { printf("    \n"); y = percent - 100; }
	if(percent >= 100)  y = percent - 100; else y = percent;
	printf("    ");  printf("Page: %d ", (uint16_t)percent);
    for(x = 0; x < y; x++)
    {   
      printf("#");
    }
      printf("\r");
      Sleep(1);
}


void animatedBar(void) {
    
    int x;
    printf("     ");
        for(x = 0; x < 40; x++)
    {
       printf("*");
       Sleep(5);
    } 
}


//========================================================================================
//        main 
//========================================================================================

 int main(int argc, char **argv) {

       #if 0
         (uint8_t)rig /= 2;
		 printf("val = %d", rig);
         rig /= 2;
		 printf("val = %d", rig);
       #endif

		char rcv, cmd[50] = {0}, * p;
		 uint8_t val = 0x80;
		const char * welcome = 
			" \n     Keys [1]-[2]-[3]-[4] to toggle Opcodes,\n\
     [h] to Command List, [P] to enter\n     Commands, [Return] to quit\n\n";
	     
     UINT oldCodePage;
	 static uint8_t inp = 0, pos = 0;
	 volatile float msCntr = 0;
     clock_t t; 
     t = clock();
		long long cntr;
        char* tString;
		time_t init = time(NULL);
        tString = ctime(&init); 
		cntr = init;

		 printf("\n         ESP8266 Server UART Terminal V.%s by 'intenseC' ", 
	     _SFVERSION); 
		 printf("\n         %s \n", tString);
     #ifdef _DEBUG 
		 printf("        *** This is an early beta version, for testing purposes only! *** \n\n\n");
	 #else
	 #endif
	
	#if 0
    oldCodePage = GetConsoleOutputCP();
    if (!SetConsoleOutputCP(65001)) {
        printf("error\n");
    }
	#endif	
     animatedBar();

      uartHandShk();
       // startComm();
	//	  espTalk('1');
 
         printf("\n\n     Hit [p] to switch to a lower port (if any),  \n\
         any other key to proceed.  \n\n "); 
        		  printf(welcome);
//========================================================================================
//         eternal  loop
//========================================================================================
	#if 1
             while(1)  {
                /* */ 
				if(_kbhit())      {    // noblocking
				rcv = _getch();
				 if(rcv == 'p' && !doOnce) {  CloseSerialPort();  portQue--; Sleep(200);
                 uartHandShk(); } else   doOnce = 1; 
					if(rcv == 'P' && !inp) { 
						inp = 1;
				 printf("\n     ~ Switching to a Manual Printing Mode. ~\n\n");

				 fgets(cmd, sizeof cmd, stdin);
                 if((p = strchr(cmd, '\n')) != NULL) *p = '\0';
    #ifndef _HELLOcHINA
							  strcat(cmd, "\r\n");
	#endif
				WriteFile(hSerial, cmd, strlen(cmd), &bytes_written, NULL);
    #ifdef _HELLOcHINA
			helloChina(0);
	#endif
				memset(cmd, 0, sizeof cmd);    //  pos = inp = 0;
			    ReadFile(hSerial, cmd, 1, &bytes_read, NULL);
                if(bytes_read)  {
                      printf("%s", cmd); 
                //      printf("%x", cmd);
                      memset(cmd, 0, sizeof cmd);
                      
				                 } inp = 0;
	            printf("\n     ~ Leaving the Manual Printing Mode. ~\n\n");
					 }
				 if(rcv != '\r')
				 {
     switch(rcv)  {
          case '1':   espTalk(rcv);    break;
	      case '2':   scaler ^= 1;     break;  
		  case '3':    //TODO: too complicated, simplify
		  case '4':
		  case 'l':
		  case 's':
		  case 'f':
		  case 'h':
          espTalk(rcv);
		  break;
				  }	 /**/
				  printf(welcome);  
				  printf("\n\r"); 
				} else  exit(1); 
		                             } 
			    ReadFile(hSerial, cmd, 1, &bytes_read, NULL);
                if(bytes_read)  {
                      // printf("%s", cmd);
                        printf("0x%02X ", cmd[0]);
                       WriteFile(hSerial, &val, 1, &bytes_written, NULL);
				                       }
              if(clock() > msCntr + t )  // tick
			  { 
				 msCntr += 25; // millisecs gap
				  if(!scaler) { 
		           draWebSockets(); 
					}
			  }
	#if 0
	          ReadFile(hSerial, cmd, 4, &bytes_read, NULL);
	          if(bytes_read == 1)
            {
                printf("\n %s", cmd); 
                WriteFile(hSerial, "\r\n", 2, &bytes_written, NULL);
            }
	#endif
	   }
    #endif
            while(1) {
    #if 1
            newP:
            if(scaler < 500) 
			  {
			 if(clock() > msCntr + t )
			  { 
				 msCntr += 25; 
				 if(!scaler) { 
		       uint32_t timeTaken = (msCntr + t) - (clock());
		            draWebSockets(); }

			  if(++dScaler > 50) { dScaler = 0;  // pause between chunks
			  if(scaler > 0) scaler--; } 
			     }
			 }
				  //   1s loop
				if(time(NULL) > cntr)   cntr += 5;
     #else
	            ReadFile(hSerial, cmd, 4, &bytes_read, NULL);
	            if(bytes_read == 1)  {
                   printf("\n received %s", cmd); 
                  }
			 if(scaler < 500) 
			{
			 if(clock() > msCntr + t ) 
			     { 
				 msCntr += 10;  
				 if(!scaler)    { 
		   uint32_t timeTaken = (msCntr + t) - (clock());
		   printf("gap is %d milliseconds\n", timeTaken); scaler = 100;   }
			  if(++dScaler > 50) { dScaler = 0;  // pause between chunks
			  if(scaler > 0) scaler--; } 
			     }
			 }	
			  if(time(NULL) > cntr ) { ; }
    #endif
    }
}









/*
1
2
3
4

5

=======
9
*/

